package exceptions;

import java.util.List;

class SimpleMath {
	float calculateAverage(List<Integer> listInteger) {
		float average = 0;
		for (Integer intIterator : listInteger) {
			average += intIterator;
		}
		average /= listInteger.size();
		return average;
	}
}
