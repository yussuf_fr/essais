package exceptions;

import java.util.List;
import java.util.ArrayList;

public class TemperatureAverage {

	/**
	 * affiche la température moyenne à partir des valeurs fournies comme arguments
	 * en ligne de commande
	 *
	 * @param args liste de températures séparées par des espaces
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SimpleMath averageListInteger = new SimpleMath();
		List<Integer> listInteger = new ArrayList<>();
		float averageTemperature = 0;
		try {
			for (String sIterator : args) {
				listInteger.add(Integer.parseInt(sIterator));
			}
			averageTemperature = averageListInteger.calculateAverage(listInteger);
			System.out.println(averageTemperature);
		}  catch (NumberFormatException e) {
			System.out.println("All arguments should be provided as numbers");
			System.exit(-1);
		} catch (ArithmeticException e) {
			System.out.println("At least one temperature should be provided");
			System.exit(-1);
		}
	}
}
