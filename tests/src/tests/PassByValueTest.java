package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class PassByValueTest {
  @Test
  public void with_primitive_type() {
    int value = 5;

    modify(value); // on transmet bien une copie de la variable
    assertEquals(5, value); // la preuve
  }

  private void modify(int copy) {
    copy = copy - 112;
  }

  @Test
  public void with_reference_type() {
    StringBuilder value = new StringBuilder();

    nullify(value); // la valeur n'est pas transmise par référence
    assertNotNull(value); // la preuve

    modify(value); // un Handle ou une référence Java autorise l'accès à l'objet sous-jacent
    assertEquals("it works as expected", value.toString());
  }

  // Un passage par référence modifierait également la variable 'value'
  private void nullify(StringBuilder copy) {
    copy = null;
  }

  private void modify(StringBuilder copy) {
    copy.append("it works as expected");
  }
}