package exScopePkgPub;

import scopePub.UnicornPub;
import scopePkg.UnicornPkg; //not visible
import scopePkg.Interf; //not visible

public class MainExScopePkg implements  {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UnicornPub unicornPub = new UnicornPub();
		System.out.println("MainExScopePub");
		System.out.println(unicornPub.power);
//		unicornPub.height = 180; // Erreur
//		unicornPub.sleep(); // Erreur
		unicornPub.run(); // Ok
		
		UnicornPkg unicornPkg = new UnicornPkg(); //n'est pas visible
//		System.out.println("MainExScopePub");
//		System.out.println(unicornPkg.power);
//		unicornPkg.height = 180;
//		unicornPkg.sleep();
		unicornPkg.run();
		
	}
}
