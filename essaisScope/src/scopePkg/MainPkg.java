package scopePkg;

public class MainPkg {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UnicornPkg unicornPkg = new UnicornPkg();
		System.out.println("MainScopePkg");
		System.out.println(unicornPkg.power); // Ok
//		unicornPkg.height = 180; // Erreur
//		unicornPkg.sleep(); // Erreur
		unicornPkg.run(); // Ok
	}
}
