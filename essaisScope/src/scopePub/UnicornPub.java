package scopePub;

public class UnicornPub {
    // propriétés
    private int height = 170;
    public String power = "Double.infinity";
    
    // méthodes
    private static void sleep() {
    }
    public void run() {
    	System.out.println("run");
    }
}
