package scopePub;

public class MainInPkg {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UnicornPub unicornPub = new UnicornPub();
		System.out.println("MainScopePub");
		System.out.println(unicornPub.power); // Ok
//		unicornPub.height = 180; // Erreur
//		unicornPub.sleep(); // Erreur
		unicornPub.run(); // Ok
	}
}
